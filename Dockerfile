FROM openjdk:14
ADD target/Music-0.0.1-SNAPSHOT.jar music.jar
ENTRYPOINT ["java", "-jar", "/music.jar"]