package Music.controllers;


import Music.data_access.CustomerRepository;
import Music.models.CountryCount;
import Music.models.Customer;
import Music.models.GenreCount;
import Music.models.SpendingCount;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomerController {
    CustomerRepository customerRepository = new CustomerRepository();

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public List<Object> getAllCustomers() {
        List<Object> out = new ArrayList<>();
        out.add(customerRepository.getAllCustomers());
        out.add(HttpStatus.OK);
        return out;
    }

    @RequestMapping(value = "/customers", method = RequestMethod.POST)
    public List<Object> addNewCustomer(@RequestBody Customer customer) {
        List<Object> out = new ArrayList<>();
        Boolean success = customerRepository.addNewCustomer(customer);
        out.add(success);
        if (!success) {
            out.add(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            out.add(HttpStatus.OK);
        }
        return out;
    }

    @RequestMapping(value = "/customers/{id}", method = RequestMethod.PUT)
    public List<Object> updateExistingCustomer(@RequestBody Customer customer) {
        List<Object> out = new ArrayList<>();
        Boolean success = customerRepository.updateExistingCustomer(customer);
        out.add(success);
        if (!success) {
            out.add(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            out.add(HttpStatus.OK);
        }
        return out;
    }

    @RequestMapping(value = "/customers/{id}", method = RequestMethod.GET)
    public List<Object> getCustomerById(@PathVariable String id) {
        List<Object> out = new ArrayList<>();
        out.add(customerRepository.getCustomerById(id));
        out.add(HttpStatus.OK);
        return out;
    }

    @RequestMapping(value = "/customers/countries", method = RequestMethod.GET)
    public List<Object> getCountryCount() {
        List<Object> out = new ArrayList<>();
        out.add(customerRepository.getCountryCount());
        out.add(HttpStatus.OK);
        return out;
    }

    @RequestMapping(value = "/customers/spenders", method = RequestMethod.GET)
    public List<Object> getHighestSpenders() {
        List<Object> out = new ArrayList<>();
        out.add(customerRepository.getHighestSpenders());
        out.add(HttpStatus.OK);
        return out;
    }

    @RequestMapping(value = "/customers/genres", method = RequestMethod.GET)
    public List<Object> getFavouriteGenres() {
        List<Object> out = new ArrayList<>();
        out.add(customerRepository.getFavouriteGenres());
        out.add(HttpStatus.OK);
        return out;
    }

    @RequestMapping(value = "/customers/{id}/popular/genre", method = RequestMethod.GET)
    public List<Object> getFavouriteGenre(@PathVariable String id) {
        List<Object> out = new ArrayList<>();
        out.add(customerRepository.getCustomersFavouriteGenre(Integer.parseInt(id)));
        out.add(HttpStatus.OK);
        return out;
    }
}
