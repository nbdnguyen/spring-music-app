package Music.controllers;

import Music.models.Track;
import org.springframework.stereotype.Controller;
import Music.data_access.SongRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import java.util.ArrayList;

@Controller
public class MusicController {
    SongRepository songRepository = new SongRepository();
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("artists", songRepository.getRanArtists());
        model.addAttribute("songs", songRepository.getRanSongs());
        model.addAttribute("playlists", songRepository.getRanPlaylists());
        return "home";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String simpleSearch(@RequestParam("query") String searched, Model model) {
        ArrayList<Track> songs = songRepository.getSongSearch(searched);
        model.addAttribute("searched", searched);
        //If the search returns no song, then the page will say so
        model.addAttribute("searchedResult", songs);
        return "search";
    }
}
