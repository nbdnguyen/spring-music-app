package Music.data_access;

import java.sql.*;
import java.util.ArrayList;

import Music.models.CountryCount;
import Music.models.Customer;
import Music.models.GenreCount;
import Music.models.SpendingCount;

public class CustomerRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    public ArrayList<Customer> getAllCustomers() {
        //Returns an array of all the customers with basic information.
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            //Makes and executes the query to get all customers and their information.
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "Select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                                    "FROM customer");
            ResultSet resultSet = preparedStatement.executeQuery();
            //Adds all the information into an ArrayList<Customer>.
            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getInt("PostalCode"),
                        resultSet.getInt("Phone"),
                        resultSet.getString("Email")
                ));
            }
            System.out.println("All customers gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            //Closes connection after running.
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return customers;
    }

    public Customer getCustomerById(String id) {
        //Returns a specific customer by id, from controller.
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "Select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                                    "FROM customer WHERE CustomerId = ?");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            customer = new Customer(
                    resultSet.getInt("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getInt("PostalCode"),
                    resultSet.getInt("Phone"),
                    resultSet.getString("Email")
            );
            System.out.println("Customer gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return customer;
    }

    public Boolean addNewCustomer(Customer customer) {
        //Adds a new customer to the database, returns true of successfully added.
        Boolean success = false;
        try {
            System.out.println(customer.getFirstName() + customer.getLastName());
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(" +
                            "CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email)" +
                            " VALUES(?,?,?,?,?,?,?)");
            preparedStatement.setString(1, String.valueOf(customer.getCustomerId()));
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, String.valueOf(customer.getPostalCode()));
            preparedStatement.setString(6, String.valueOf(customer.getPhone()));
            preparedStatement.setString(7, customer.getEmail());
            int result = preparedStatement.executeUpdate();
            success = (result != 0); // if res = 1; true
            System.out.println("Customer added.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return success;
    }

    public Boolean updateExistingCustomer(Customer customer) {
        //Updates an existing customer, returns true if successfully updated.
        Boolean success = false;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customer SET CustomerId=?, FirstName=?, LastName=?, " +
                            "Country=?, PostalCode=?, Phone=?, Email=?" +
                            " WHERE CustomerId=?");
            preparedStatement.setString(1, String.valueOf(customer.getCustomerId()));
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, String.valueOf(customer.getPostalCode()));
            preparedStatement.setString(6, String.valueOf(customer.getPhone()));
            preparedStatement.setString(7, customer.getEmail());
            preparedStatement.setString(8, String.valueOf(customer.getCustomerId()));

            int result = preparedStatement.executeUpdate();
            success = (result != 0); // if res = 1; true
            System.out.println("Customer updated");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return success;
    }

    public ArrayList<CountryCount> getCountryCount() {
        //Returns a list of countries and their corresponding amount of customers in that country.
        ArrayList<CountryCount> countryCounts = new ArrayList<CountryCount>();
        CountryCount country = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "SELECT Country, COUNT(*) as Count FROM customer GROUP BY Country ORDER BY Count DESC");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                country = new CountryCount(
                        resultSet.getString("Country"),
                        resultSet.getInt("Count")
                        );
                countryCounts.add(country);
            }
            System.out.println("Country counts gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return countryCounts;
    }

    public ArrayList<SpendingCount> getHighestSpenders() {
        //Returns a list of the customers with their total amount spent, in descending order of the total amoount.
        ArrayList<SpendingCount> highestSpenders = new ArrayList<SpendingCount>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "SELECT customer.CustomerId, FirstName, LastName, Total FROM customer " +
                                    "INNER JOIN invoice ON customer.CustomerId = invoice.CustomerId " +
                                    "GROUP BY Total ORDER BY Total DESC");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                highestSpenders.add(new SpendingCount(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getInt("Total")
                ));
            }
            System.out.println("Highest spenders gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return highestSpenders;
    }

    public ArrayList<GenreCount> getFavouriteGenres() {
        //Returns a list of all the customers with their favourite music genre.
        ArrayList<GenreCount> favouriteGenres = new ArrayList<GenreCount>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "SELECT customer.CustomerId, FirstName, LastName, g.GenreId, g.Name, COUNT(*) as Count " +
                                    "FROM customer " +
                                    "INNER JOIN invoice AS i ON customer.CustomerId = i.CustomerId " +
                                    "INNER JOIN invoiceLine AS il ON i.InvoiceId = il.InvoiceId " +
                                    "INNER JOIN track AS t ON il.TrackId = t.TrackId " +
                                    "INNER JOIN genre AS g ON t.GenreId = g.GenreId " +
                                    "GROUP BY customer.customerId, g.GenreId, g.Name  " +
                                    "ORDER BY customer.CustomerId, Count DESC");
            ResultSet resultSet = preparedStatement.executeQuery();
            int id = 0;
            while (resultSet.next()) {
                if (id == resultSet.getInt("CustomerId")) {
                } else {
                    favouriteGenres.add(new GenreCount(
                            resultSet.getInt("CustomerId"),
                            resultSet.getString("FirstName"),
                            resultSet.getString("LastName"),
                            resultSet.getString("Name")
                    ));
                    id = resultSet.getInt("CustomerId");
                }
            }
            System.out.println("Favourite genres gathered.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return favouriteGenres;
    }

    public GenreCount getCustomersFavouriteGenre(int id) {
        //Returns a specific customer and their favourite genre, id from controller.
        ArrayList<GenreCount> array = this.getFavouriteGenres();
        GenreCount out = null;
        for (GenreCount i : array) {
            if (id == i.getCustomerId()) {
                out = i;
                break;
            }
        }
        return out;
    }
}
