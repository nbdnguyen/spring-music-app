package Music.data_access;

import java.sql.*;
import java.util.ArrayList;
import Music.models.Track;

public class SongRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    public ArrayList<Track> getSongSearch(String songSearched) {
        //Returns the first song in the database where the title contains the searched term in any place.
        ArrayList<Track> searchResult = new ArrayList<Track>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            //Creating and executing a query to search for the term through all songs.
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "Select t.Name as title, a.Title as album, g.Name as genre, art.Name as artist " +
                            "FROM track as t " +
                            "INNER JOIN album as a on t.AlbumId = a.AlbumId " +
                            "INNER JOIN genre as g on t.GenreId = g.GenreId " +
                            "INNER JOIN artist as art on a.ArtistId = art.ArtistId " +
                            "WHERE upper(t.Name) LIKE ?");
            preparedStatement.setString(1,"%" + songSearched.toUpperCase() + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            //Returns a song that is the track we return.
            while (resultSet.next()) {
                searchResult.add(
                        new Track(
                                resultSet.getString("title"),
                                resultSet.getString("album"),
                                resultSet.getString("genre"),
                                resultSet.getString("artist")
                ));
            }
            System.out.println("Song found.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return searchResult;
    }

    public ArrayList<String> getRanArtists() {
        //Returns a list of five random artists.
        ArrayList<String> ranArtists = new ArrayList<String>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("Select Name FROM artist ORDER BY random() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ranArtists.add(
                        resultSet.getString("Name"));
            }
            System.out.println("Random artists acquired.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return ranArtists;
    }

    public ArrayList<String> getRanSongs() {
        //Returns a list of five random songs.
        ArrayList<String> ranSongs = new ArrayList<String>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("Select Name FROM track ORDER BY random() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ranSongs.add(
                        resultSet.getString("Name"));
            }
            System.out.println("Random songs acquired.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return ranSongs;
    }

    public ArrayList<String> getRanPlaylists() {
        //Returns a list of five random playlists.
        ArrayList<String> ranPlaylists = new ArrayList<String>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("Select Name FROM playlist ORDER BY random() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ranPlaylists.add(
                        resultSet.getString("Name"));
            }
            System.out.println("Random playlists acquired.");
        } catch (Exception e) {
            System.out.println("There has been an error: " + e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println("Could not close connection: " + e.toString());
            }
        }
        return ranPlaylists;
    }
}
