package Music.models;

public class GenreCount {
    private int customerId;
    private String firstName;
    private String lastName;
    private String genre;
    public GenreCount(int customerId, String firstName, String lastName, String genre) {
        this.customerId = customerId; this.firstName = firstName;
        this.lastName = lastName; this.genre = genre;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGenre() {
        return genre;
    }
}
