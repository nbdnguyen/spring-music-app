package Music.models;

public class SpendingCount {
    private int customerId;
    private String firstName;
    private String lastName;
    private int total;

    public SpendingCount(int customerId, String firstName, String lastName, int total) {
        this.customerId = customerId; this.firstName = firstName; this.lastName = lastName; this.total = total;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getTotal() {
        return total;
    }
}
