package Music.models;

public class Track {
    private String title;
    private String album;
    private String genre;
    private String artist;

    public Track(String title, String album, String genre, String artist) {
        this.title = title; this.album = album; this.genre = genre; this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public String getAlbum() {
        return album;
    }

    public String getGenre() {
        return genre;
    }

    public String getArtist() {
        return artist;
    }

    @Override
    public String toString() {
        return "'" + title + "' by " + artist;
    }
}
